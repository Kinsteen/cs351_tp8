#include <stdio.h>
#include <stdlib.h>

#include "Matrice.h"

int main(void) {
    Matrice *test1 = NULL;
    Matrice *test2 = NULL;
    Matrice *test3 = NULL;
    Matrice *res = NULL;
    Matrice *res2 = NULL;
    createMatrice(&test1, 4, 3);
    createMatrice(&test2, 4, 3);
    createMatrice(&test3, 3, 4);
    createMatrice(&res, 4, 3);
    createMatrice(&res2, 3, 3);

    addElement(test1, 0,0,5);
    addElement(test1, 1,1,6);
    addElement(test1, 3,2,4);
    addElement(test1, 0,2,3);

    addElement(test2, 0,0,1);
    addElement(test2, 1,1,2);
    addElement(test2, 1,2,2);
    addElement(test2, 0,2,3);
    
    addElement(test3, 0,0,1);
    addElement(test3, 1,1,2);
    addElement(test3, 2,2,2);
    addElement(test3, 0,2,3);

    printf("--Test1--\n");
    printMatrice(test1);

    addMatrice(test1, test2, res);

    printf("--ADD--\n");
    printMatrice(res);

    printf("--MUL--\n");
    printMatrice(test1);
    printMatrice(test3);
    mulMatrice(test1, test3, res2);
    printMatrice(res2);

    freeMatrice(&test1);
    freeMatrice(&test2);
    freeMatrice(&test3);
    freeMatrice(&res);
    freeMatrice(&res2);
    return 0;
}