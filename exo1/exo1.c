#include <stdlib.h>
#include <stdio.h>

typedef struct element element;
struct element {
    int valeur ; /* valeur de l’élément */
    element* suivant; /* adresse du successeur */
};
typedef element* liste;

void insererElement(int x, liste *L) {  //ajoute un element a la liste en gardant celle ci trié 
    element *new;
    element* current;
    element* precedent;

    if (L != NULL) {
        current = *L;
        precedent = current;
        new = (element*) malloc(sizeof(element));
        new->valeur = x;
        new->suivant = NULL;

        while ((current->valeur < x) && (current->suivant != NULL)) {
            precedent = current;
            current = current->suivant;
        }

        if (current->valeur < x) {
            current->suivant = new;
        } else {
            if (precedent == current) { //cas ou il faut inserer en tête de liste 
                new->suivant = current;
                *L = new;
            } else {
                precedent->suivant = new;
                new->suivant = current;
            }
        }
    }
}

void afficherliste(liste L) {
    while (L != NULL) {
        printf("valeur : %d\n", L->valeur);
        L = L->suivant;
    }
}

void supprimerElement(int x, liste *L) {
    element *current;
    element *precedent;
    if (L != NULL) {
        current = *L;
        precedent = current;

        while ((x != 0) && (current->suivant != NULL)) {
            x--;
            precedent = current;
            current = current->suivant;
        }

        if (precedent == current) {   //si on veut supprimer la tête de liste
            *L = (*L)->suivant;
            free(current);
        } else {
            precedent->suivant = current->suivant;
            free(current);
        }
    }
}

void init(int value, liste *L) { // permet d'initialiser une liste vide avec une premiere valeur
    (*L) = (element*) malloc(sizeof(element));
    
    (*L) -> valeur = value;
    (*L) -> suivant = NULL;
}

int main(void) {
    liste L = NULL; 
    init(5, &L);
    insererElement(6, &L);
    insererElement(7, &L);
    insererElement(4, &L);
    insererElement(1, &L);
    afficherliste(L);

    printf("suppression de l'element a l'indice 1 :\n");
    supprimerElement(1, &L);
    afficherliste(L);

    printf("suppression de la tête de liste :\n");
    supprimerElement(0, &L);
    afficherliste(L);

   /* ajoutElement( 6, L);
    supprimerElement(L);
    afficherliste(L);
    ajoutElement( 10, L);
    afficherliste(L);
    supprimerElement(L);
    afficherliste(L);
*/
   
}