#include <stdio.h>
#include <stdlib.h>

#include "Matrice.h"

void createMatrice(Matrice **mat, int width, int height) {
    *mat = malloc(sizeof(MatriceElement *) * 2);
    (*mat)->height = height;
    (*mat)->width = width;


    for (int i = 0; i < width; i++) {
        MatriceElement *x = malloc(sizeof(MatriceElement)); // On crée l'élement colonne, qui aura l'information du premier bloc dans la colonne
        x->x = i;
        x->y = -1;
        x->val = 0;
        x->pointerDown  = NULL;
        x->pointerRight = NULL;

        if ((*mat)->right == NULL) { // First
            (*mat)->right = x;
        } else {
            MatriceElement *temp = (*mat)->right;

            while (temp->pointerRight != NULL) {
                temp = temp->pointerRight;
            }

            temp->pointerRight = x;
        }
    }

    for (int i = 0; i < height; i++) {
        MatriceElement *y = malloc(sizeof(MatriceElement));
        y->x = -1;
        y->y = i;
        y->val = 0;
        y->pointerDown  = NULL;
        y->pointerRight = NULL;


        if ((*mat)->down == NULL) { // First
            (*mat)->down = y;
        } else {
            MatriceElement *temp = (*mat)->down;

            while (temp->pointerDown != NULL) {
                temp = temp->pointerDown;
            }

            temp->pointerDown = y;
        }
    }
}

void freeMatrice(Matrice **mat) {
    MatriceElement *tempX = NULL;
    MatriceElement *tempY = NULL;

    if ((*mat)->right != NULL) { // On commence par supprimer les élements colonne
        tempX = (*mat)->right;

        while (tempX != NULL) {
            MatriceElement *toFree = tempX;
            tempX = tempX->pointerRight;
            free(toFree);
        }
    }

    if ((*mat)->down != NULL) { // Puis les élements lignes, tout en supprimant les blocs associés
        tempY = (*mat)->down;

        while (tempY != NULL) {
            MatriceElement *colTemp = tempY->pointerRight;
            while (colTemp != NULL) {
                MatriceElement *toFree = colTemp;
                colTemp = colTemp->pointerRight;
                free(toFree);
            }
            MatriceElement *toFree = tempY;
            tempY = tempY->pointerDown;
            free(toFree);
        }
    }

    free(*mat); // On oublie pas de free la matrice en elle même
}

void addElement(Matrice *mat, int x, int y, int val) {
    if (x >= mat->width || y >= mat->height) { // On vérifie si on essaie pas de créer un bloc trop loin.
        fprintf(stderr, "Index out of range!\n");
        return;
    }
    MatriceElement *tempX = mat->right;
    MatriceElement *tempY = mat->down;

    for (int i = 0; i < x; i++) {
        tempX = tempX->pointerRight; // On bouge le bloc "colonne" vers la droite pour l'aligner avec l'endroit d'insertion
    }

    for (int i = 0; i < y; i++) {
        tempY = tempY->pointerDown; // On bouge le bloc "ligne" vers le bas pour l'aligner avec l'endroit d'insertion
    }

    // On parcourt la colonne pour arriver juste en face de l'endroit d'insertion
    while (tempX->pointerDown  != NULL && tempX->pointerDown->y  < y) { tempX = tempX->pointerDown; }
    while (tempY->pointerRight != NULL && tempY->pointerRight->x < x) { tempY = tempY->pointerRight; }

    if (tempX->pointerDown != NULL && tempY->pointerRight != NULL) { // Un bloc existe déjà a cet endroit, on va alors juste remplacer sa valeur
        if (tempX->pointerDown->y == y && tempY->pointerRight->x == x) {
            tempX->pointerDown->val = val;
            return; // On finit la fonction ici
        }
    }
    
    MatriceElement *newElement = malloc(sizeof(MatriceElement));
    MatriceElement *oldDown = tempX->pointerDown;
    MatriceElement *oldRight = tempY->pointerRight;
    // Maintenant, nous avons tempX et tempY qui sont à la bonne position, c'est à dire au bloc juste avant l'insertion.
    tempX->pointerDown = newElement; // On relie le nouveau bloc au bloc précédent
    tempY->pointerRight = newElement;
    newElement->x = x;
    newElement->y = y;
    newElement->val = val;
    newElement->pointerDown = oldDown; // On relie tous les pointeurs au bon endroit
    newElement->pointerRight = oldRight;
}

void printMatrice(Matrice *mat) {
    MatriceElement *tempY = mat->down;
    MatriceElement *tempX = tempY->pointerRight;
    int oldX = 0;

    while (tempY != NULL) {
        while (tempX != NULL) {
            for (;oldX < tempX->x; oldX++) // On affiche des 0 entre les vides
                printf("0 ");
            printf("%d ", tempX->val);
            oldX++;
            tempX = tempX->pointerRight;
        }
        for (;oldX < mat->width; oldX++) // On finit la ligne de 0
            printf("0 ");
        printf("\n");
        tempY = tempY->pointerDown;
        oldX = 0;
        if (tempY != NULL) {
            tempX = tempY->pointerRight;
        }
    }
}

int searchValue(Matrice *mat, int x, int y) { // Retourne la valeur de la matrice a (x,y)
    MatriceElement *column = mat->down;
    MatriceElement *temp = column->pointerRight;
    int value = 0;

    for (int i = 0; i < y; i++) {
        column = column->pointerDown;
    }

    temp = column->pointerRight;

    while (temp != NULL && temp->x <= x) {
        if (temp->x == x && temp->y == y) {
            value = temp->val;
        }
        temp = temp->pointerRight;
    }

    return value;
}

// On suppose que les tailles des matrices sont les mêmes
void addMatrice(Matrice *mat1, Matrice *mat2, Matrice *res) {
    for (int x = 0; x < res->width; x++) {
        for (int y = 0; y < res->height; y++) {
            // Cherchons les élements
            int valMat1 = searchValue(mat1, x, y);
            int valMat2 = searchValue(mat2, x, y);
            if (valMat1 != 0 || valMat2 != 0) {
                addElement(res, x, y, valMat1 + valMat2);
            }
        }
    }
}

// On suppose aussi que les tailles sont correctes
void mulMatrice(Matrice *mat1, Matrice *mat2, Matrice *res) {
    for (int x = 0; x < res->width; x++) {
        for (int y = 0; y < res->height; y++) {
            MatriceElement *column = mat1->down;
            for (int i = 0; i < y; i++) column = column->pointerDown;
            MatriceElement *temp = column->pointerRight;

            int sum = 0;
            while (temp != NULL) {
                sum += temp->val * searchValue(mat2, x, temp->x);
                temp = temp->pointerRight;
            }

            if (sum != 0) {
                addElement(res, x, y, sum);
            }
        }
    }
}

/*

Imaginons une multiplication de deux matrices 1000*1000.
Pour chaque case dans le cas de la multiplication classique, il y a 1000 multiplications.
Donc, nous avons 1000^3 multiplications, soit 1 million.

Dans le cas de la matrice creuse, nous ne multiplions pas les cases ou il y a un zéro.
Comme il n'y a que 10% d'élément non nul, nous avons 100 multiplications par case, on on divise par 10 le nombre total de multiplications
Soit 100 000 multiplications au lieu d'un million.

*/
