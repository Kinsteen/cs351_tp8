#ifndef MATRICE_H
#define MATRICE_H

/*
If x or y == -1, it's the column or row descriptor
*/
typedef struct mElement {
    int x;
    int y;
    int val;
    struct mElement *pointerDown;
    struct mElement *pointerRight;
} MatriceElement;

/*
 -->x
|
V
y
*/
typedef struct {
    MatriceElement *down; // y
    MatriceElement *right; // x
    int width;
    int height;
} Matrice;

void createMatrice(Matrice **mat, int width, int height);
void freeMatrice(Matrice **mat);
void addElement(Matrice *mat, int x, int y, int val);
void printMatrice(Matrice *mat);
void addMatrice(Matrice *mat1, Matrice *mat2, Matrice *res);
void mulMatrice(Matrice *mat1, Matrice *mat2, Matrice *res);

#endif